import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


import { config } from '../config';


/*
  Generated class for the RouteProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RouteProvider {

  constructor(public http: Http) {
    console.log('Hello RouteProvider Provider');
  }
 
  get(){
      let url = config.SERVICES_LOCATION+"Index/DumpData";
      return this.http.get(url);
  }

  getById(id){
      let url = config.SERVICES_LOCATION+"Index/ById/?id="+id;
      return this.http.get(url);
  }

  post(data){

    let url = config.SERVICES_LOCATION+"Index/Index";
    let newData = this.jsonToUrlParams(data);
    return this.http.post(url,newData);

  }

  jsonToUrlParams(json){
    return Object.keys(json)
    .map( key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]) )
    .join("&");
  }

}
